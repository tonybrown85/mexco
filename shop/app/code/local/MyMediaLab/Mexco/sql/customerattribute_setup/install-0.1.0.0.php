<?php
/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

// new attribute name is 'profitcode'
// Type Integer
$installer->addAttribute("customer", "profitcode",  array(
    "type"     => "varchar",
    "label"    => "Customer Code",
    "input"    => "text",
    "visible"  => true,
    "required" => false,
    "unique"   => true,
    "note"     => "Prof.IT Customer Code"
 ));

$attribute       = Mage::getSingleton("eav/config")->getAttribute("customer", "profitcode");

$used_in_forms   = array();

$used_in_forms[] = "adminhtml_customer";

$attribute->setData("used_in_forms", $used_in_forms)
          ->setData("is_used_for_customer_segment", true)
          ->setData("is_system", 0)
          ->setData("is_user_defined", 1)
          ->setData("is_visible", 0)
          ->setData("sort_order", 100);

// Save Attribute
$attribute->save();

// ensure that last name is not a required field
$installer->updateAttribute('customer', 'lastname', 'is_required', 0);
$installer->updateAttribute('customer_address', 'lastname', 'is_required', 0);
$installer->updateAttribute('customer_address', 'country_id', 'is_required', 0);
$installer->updateAttribute('customer_address', 'city', 'is_required', 0);
$installer->updateAttribute('customer_address', 'postcode', 'is_required', 0);
$installer->updateAttribute('customer_address', 'telephone', 'is_required', 0);

$installer->run('UPDATE customer_eav_attribute SET validate_rules = \'a:1:{s:15:"max_text_length";i:255;}\' WHERE customer_eav_attribute.attribute_id IN (7, 22, 26)');

$installer->endSetup();
