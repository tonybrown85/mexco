<?php
// Magento does not autoload controllers (of course!) so you have to include the original here to extend it.
require_once Mage::getModuleDir('controllers', 'Mage_Customer') . DS . "AccountController.php";

class MyMediaLab_Mexco_AccountController extends Mage_Customer_AccountController
{
    private $database;

    /**
     * We're trying to disable manual customer account creation, so just shortcut those methods to redirect.
     */
    public function createAction()
    {
        $this->_getSession()->addError("Manual customer registration is disabled.");
        $this->_redirect('*/*/');
    }

    public function createPostAction()
    {
        $this->createAction();
    }

    public function loginPostAction()
    {
        $session = $this->_getSession();
        $details = $this->getRequest()->getPost('login');

        if (!isset($details['username']) || !isset($details['password'])) {
            throw new \Exception("Username or password not supplied");
        }

        $database = $this->_oglDatabase();

        if (!$this->oglLogin($details)) {
            throw new \Exception("Username or password incorrect");
        }

        if ($this->overCreditLimit($details)) {
            $session->addError("Sorry, you have reached your credit limit.");
            $this->_redirect('*/*/');
            return;
        }

        if (!$this->loginByEmail($_POST['login']['username'])) {
            throw new \Exception("Failed to create session");
        }

        $this->_redirect('*/*/');
        return;
    }

    /**
     * Change customer password action
     *
     * Ufortunately we've had to override this entire function. Gutted.
     *
     * Changed bits are:
     *   intercept the change password request and change the stored
     *   password in the OGL database instead.
     */
    public function editPostAction()
    {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('*/*/edit');
        }

        if ($this->getRequest()->isPost()) {
            /** @var $customer Mage_Customer_Model_Customer */
            $customer = $this->_getSession()->getCustomer();

            /** @var $customerForm Mage_Customer_Model_Form */
            $customerForm = $this->_getModel('customer/form');
            $customerForm->setFormCode('customer_account_edit')
                ->setEntity($customer);

            $customerData = $customerForm->extractData($this->getRequest());

            $errors = array();
            $customerErrors = $customerForm->validateData($customerData);
            if ($customerErrors !== true) {
                $errors = array_merge($customerErrors, $errors);
            } else {
                $customerForm->compactData($customerData);
                $errors = array();

                // If password change was requested then add it to common validation scheme
                if ($this->getRequest()->getParam('change_password')) {
                    $errors = array_merge($errors, $this->_validatePasswordSubmission());
                }

                // Validate account and compose list of errors if any
                $customerErrors = $customer->validate();
                if (is_array($customerErrors)) {
                    $errors = array_merge($errors, $customerErrors);
                }
            }

            if (!empty($errors)) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost());
                foreach ($errors as $message) {
                    $this->_getSession()->addError($message);
                }
                $this->_redirect('*/*/edit');
                return $this;
            }

            try {
                $customer->cleanPasswordsValidationData();
                $customer->save();
                $this->_getSession()->setCustomer($customer)
                    ->addSuccess($this->__('The account information has been saved.'));

                $this->_redirect('customer/account');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
                    ->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Cannot save the customer.'));
            }
        }

        $this->_redirect('*/*/edit');
    }

    private function oglLogin(array $details)
    {
        $statement = $this->_oglDatabase()->prepare('SELECT password FROM WebAuth WHERE username = ?');

        if (!$statement->execute([$details['username']])) {
            throw new \Exception(json_encode($statement->errorInfo()));
        }

        $res = $statement->fetch(\PDO::FETCH_OBJ);
        if (!$res) {
            return false;
        }

        // lol OGL.
        return ($res->password === md5($details['password']));
    }

    private function overCreditLimit(array $details)
    {
        $statement = $this->_oglDatabase()->prepare(
            'SELECT (clim - balt) AS remaining FROM WebCustomers WHERE cref = (SELECT cref FROM WebAuth WHERE username = ?)'
        );

        if (!$statement->execute([$details['username']])) {
            throw new \Exception(json_encode($statement->errorInfo()));
        }

        $res = $statement->fetch(\PDO::FETCH_OBJ);
        if (!$res) {
            return false;
        }

        return ($res->remaining <= 0);
    }

    private function loginByEmail($email)
    {
        $customer = \Mage::getModel('customer/customer');
        $customer->setWebsiteId(\Mage::app()->getStore()->getWebsiteId()); // the website must be set here again!!!
        $customer->loadByEmail($email);

        if ($customer->getId()) {
            $session = \Mage::getSingleton('customer/session');
            $session->setCustomerAsLoggedIn($customer);
            return true;
        }

        return false;
    }

    /**
     * This relies heavily on the module's OGL config being present and correct.
     * Expect trouble if you change any of that!
     */
    private function _oglDatabase()
    {
        if ($this->database) {
            return $this->database;
        }

        $connection = \Mage::getStoreConfig('mexco_options/ogl', \Mage::app()->getStore());
        $dsn = "mysql:host={$connection['db_host']};dbname={$connection['db_name']}";

        $this->database = new \PDO($dsn, $connection['db_user'], $connection['db_password']);
        return $this->database;
    }

    private function _validatePasswordSubmission()
    {
        $errors = [];

        $currPass   = (string) $this->getRequest()->getPost('current_password');
        $newPass    = (string) $this->getRequest()->getPost('password');
        $confPass   = (string) $this->getRequest()->getPost('confirmation');

        if (strlen($currPass) === 0) {
            $errors[] = "Current password cannot be empty.";
        }
        if (strlen($newPass) === 0) {
            $errors[] = "New password cannot be empty.";
        }
        if (strlen($newPass) > 0 && strlen($confPass) === 0) {
            $errors[] = "Password confirmation cannot be empty.";
        }
        if ($newPass !== $confPass) {
            $errors[] = "New Passwords entered must match";
        }

        // if we've failed any validation at this point just bail out
        if (count($errors)) {
            return $errors;
        }

        $customer  = $this->_getSession()->getCustomer();
        $statement = $this->_oglDatabase()->prepare('SELECT password FROM WebAuth WHERE username = ?');

        if (!$statement->execute([$customer->getEmail()])) {
            throw new \Exception("No such user or something");
        }

        $res = $statement->fetch(\PDO::FETCH_OBJ);
        if (!$res) {
            return false;
        }
        $oldPass = $res->password;

        if (md5($currPass) !== $oldPass) {
            $errors[] = $this->__('Invalid current password');
        } else {
            $statement = $this->_oglDatabase()->prepare('UPDATE WebAuth SET password = ? WHERE username = ?');

            if (!$statement->execute([md5($newPass), $customer->getEmail()])) {
                $errors[]= "Failed to change password";
            }
        }

        return $errors;
    }
}
