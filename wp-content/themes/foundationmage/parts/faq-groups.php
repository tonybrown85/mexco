<?php $faqGroups = get_field('faq_group'); ?>

<?php if(!empty($faqGroups)): ?>

<?php $groupCounter = 0; ?>
	<div id="faq-groups">

		<?php foreach($faqGroups as $faqGroup): ?>
		
		<?php $enableIssues = $faqGroup['issues']; ?>
		<?php $issuesTitle = $faqGroup['issues_title']; ?>
		<?php $issuesList = $faqGroup['issues_list']; ?>
		
			<div class="faq-group">

				<?php ++$groupCounter; ?>
				
				<?php if(empty($enableIssues)): ?><div class="toggle-all-wrapper"><?php endif; ?>
				<h2><?php echo $faqGroup['group_title']; ?></h2>
				<?php if(empty($enableIssues)): ?></div><?php endif; ?>
				
				<?php if(!empty($enableIssues)): ?>
				
					<?php if($issuesTitle): ?>
					
						<h3><?php echo $issuesTitle; ?></h3>
						
					<?php endif; ?>
					
					<?php if(!empty($issuesList)): ?>
					
					<ul class="common-issues-list">
					
						<?php foreach($issuesList as $issuesItem): ?>
						
							<li><?php echo $issuesItem['text']; ?></li>
						
						<?php endforeach; ?>
						
					</ul>
					
					<?php endif; ?>
					
					<?php if(!empty($enableIssues)): ?><div class="toggle-all-wrapper"><h4 class="toggle-all-binder">Common Issues</h4></div><?php endif; ?>

				
				<?php endif; ?>
				
				<ul class="accordion faq-accordion" data-accordion>
					<?php if(!empty($faqGroup['group_items'])): ?>
					<?php $groupItemCounter = 0; ?>
						<?php foreach($faqGroup['group_items'] as $groupItem): ?>
						<?php ++$groupItemCounter; ?>
							<li class="accordion-navigation">
								<a class="accordion-item-title" href="#panel<?php echo $groupItemCounter; ?><?php echo $groupCounter; ?>"><?php echo $groupItem['question']; ?></a>
								<div id="panel<?php echo $groupItemCounter; ?><?php echo $groupCounter; ?>" class="content <?php if($groupItemCounter == 1): ?>active<?php endif; ?>">
									<?php echo $groupItem['answer']; ?>
								</div>
							</li>
						<?php endforeach; ?>
					<?php endif; ?>
				</ul>		
			</div>
		<?php endforeach; ?>
		
	</div>

<?php endif; ?>
