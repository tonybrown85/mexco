<?php
$_helper = Mage::helper('catalog/category');
$_categories = $_helper->getStoreCategories(false, true, false);
$_categories->addAttributeToSelect('image');
?>

<?php if (count($_categories) > 0): ?>

	<ul class="sidebar-product-categories">
		<?php foreach($_categories as $_category): ?>
			<li><a href="<?php echo $_helper->getCategoryUrl($_category); ?>"><?php echo $_category->getName(); ?></a></li>
		<?php endforeach; ?>
	</ul>
	
<?php endif; ?>