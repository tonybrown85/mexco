<?php

$args = array(
	'post_type' => 'special-offer',
	'posts_per_page' => 24,
	'orderby' => 'date',
	'order'   => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'areas',
			'field' => 'term_id',
			'terms' => array(8),
		)
	)
);

?>
<?php $loop = new WP_Query( $args ); ?>

	<div class="offers-header">
	
		<div class="row">
		
			<div class="small-12 columns">
			
				<ul>

					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					
						<li><?php echo get_the_title(); ?></li>

					<?php endwhile; ?>
				
				</ul>
				
			</div>
			
		</div>
		
	</div>
	
	<?php wp_reset_postdata() . wp_reset_query(); ?>
	<?php do_action( 'foundationpress_after_offers' ); ?>