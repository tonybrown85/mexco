<div class="row">

	<div class="columns col5">
		<h3>About Mexco</h3>
		<?php echo foundationpress_footer_about(); ?>
		<h3>Downloads</h3>
		<?php echo foundationpress_footer_downloads(); ?>
	</div>
	
	<div class="columns col5">
		<h3>Categories</h3>
		<?php if(is_page() || is_single()): ?><?php echo the_block('catalog.footer'); ?><?php endif; ?>
	</div>
	
	<div class="columns col5">
		<h3>Customer Services</h3>
		<?php echo foundationpress_footer_customer_services(); ?>
	</div>
	
	<div class="columns col5">
		<a class="footer-logo" href="<?php echo home_url(); ?>"><img class="site-logo" src="<?php the_field('site_logo', 'option'); ?>" alt="<?php bloginfo( 'name' ); ?>" /></a>
		<?php $footerContact = get_field('contact_details', 'option'); ?>
		<?php $footerContactCounter = 0; ?>
		<?php if(!empty($footerContact)): ?>
			<?php foreach($footerContact as $footerContactItem): ?>
			<?php $footerContactCounter++; ?>
				<a id="footer-contact-item-<?php echo $footerContactCounter; ?>" class="footer-contact-item" href="<?php echo $footerContactItem['contact_details_link']; ?>"><?php if($footerContactItem['contact_details_icon']): ?><?php echo $footerContactItem['contact_details_icon']; ?><?php endif; ?><?php echo $footerContactItem['contact_details_label']; ?></a>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	
	<div class="columns col5">
		<h3>Join The Conversation</h3>
		<?php $socialNetworks = get_field('social_networks', 'option'); ?>
		<?php if(!empty($socialNetworks)): ?>
		
			<?php foreach($socialNetworks as $socialItem): ?>
			
				<a class="footer-social-link" href="<?php echo $socialItem['social_link']; ?>"><?php echo $socialItem['social_icon']; ?><span class="footer-social-link-label"><?php echo $socialItem['social_label']; ?></span></a>
			
			<?php endforeach; ?>
		
		<?php endif; ?>

	</div>
	
</div>