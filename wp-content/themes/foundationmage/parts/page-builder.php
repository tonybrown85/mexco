<?php if( have_rows('page_builder') ): ?>

	<div class="row">

	<?php while ( have_rows('page_builder') ) : the_row(); ?>

		<?php if( get_row_layout() == 'title' ): ?>
		
			<div class="small-12 columns">
			
				<div class="pb-title">
				
					<?php if(!empty(get_sub_field('title'))): ?><h2><?php echo get_sub_field('title'); ?></h2><?php endif; ?>
				
				</div>
				
			</div>

		<?php elseif( get_row_layout() == 'image_title_content' ): ?>
		
			<div<?php if(!empty(get_sub_field('column_size'))): ?> class="<?php echo get_sub_field('column_size'); ?>"<?php endif; ?>>
			
				<div class="pb-image-title-content">
				
					<?php if(!empty(get_sub_field('image'))): ?><img src="<?php echo get_sub_field('image'); ?>" alt="<?php echo get_sub_field('title'); ?>" /><?php endif; ?>
					<?php if(!empty(get_sub_field('title'))): ?><h3><?php echo get_sub_field('title'); ?></h3><?php endif; ?>
					<?php if(!empty(get_sub_field('content'))): ?><p><?php echo get_sub_field('content'); ?></p><?php endif; ?>
				
				</div>
			
			</div>
				
		<?php elseif( get_row_layout() == 'title_content' ): ?>
		
			<div<?php if(!empty(get_sub_field('column_size'))): ?> class="<?php echo get_sub_field('column_size'); ?>"<?php endif; ?>>
			
				<div class="pb-title-content">
				
					<?php if(!empty(get_sub_field('title'))): ?><h2><?php echo get_sub_field('title'); ?></h2><?php endif; ?>
					<?php if(!empty(get_sub_field('content'))): ?><p><?php echo get_sub_field('content'); ?></p><?php endif; ?>
					
				</div>
			
			</div>
				
		<?php elseif( get_row_layout() == 'title_image' ): ?>
		
			<div<?php if(!empty(get_sub_field('columns_size'))): ?> class="<?php echo get_sub_field('columns_size'); ?>"<?php endif; ?>>
			
				<div class="pb-title-image">
				
					<?php if(!empty(get_sub_field('title'))): ?><h3><?php echo get_sub_field('title'); ?></h3><?php endif; ?>
					<?php if(!empty(get_sub_field('image'))): ?><img src="<?php echo get_sub_field('image'); ?>" alt="<?php echo get_sub_field('title'); ?>" /><?php endif; ?>
				
				</div>
			
			</div>
				
		<?php elseif( get_row_layout() == 'list_with_content' ): ?>
		
			<div class="small-12 columns">
			
				<div class="pb-list-content">
				
					<?php if(!empty(get_sub_field('item_title'))): ?><h3><?php echo get_sub_field('item_title'); ?></h3><?php endif; ?>
					<?php if(!empty(get_sub_field('item_content'))): ?><p><?php echo get_sub_field('item_content'); ?></p><?php endif; ?>
				
				</div>
			
			</div>
				
		<?php elseif( get_row_layout() == 'content_divider' ): ?>
		
			<div class="small-12 columns">
			
				<?php if(!empty(get_sub_field('enable_divider'))): ?>
			
					<hr>
				
				<?php endif; ?>
			
			</div>

		<?php endif; ?>

	<?php endwhile; ?>

	<?php else : ?>

	<h3>There isn't any content!? Feel free to add some building blocks</h3>

	</div>

<?php endif; ?>