<?php if(Mage::getSingleton('customer/session')->isLoggedIn()): ?>

	<?php

	$args = array(
		'post_type' => 'special-offer',
		'posts_per_page' => 24,
		'tax_query' => array(
			array(
				'taxonomy' => 'areas',
				'field' => 'term_id',
				'terms' => array(9),
			)
		)
	);

	?>
	
	<?php $loop = new WP_Query( $args ); ?>

	<div id="ion-product-category-slider">

		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
		
			<?php $offerThumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium-banner' ); ?>
			<?php $offerThumbUrl = $offerThumb['0']; ?>
		
			<div><img src="<?php echo $offerThumbUrl; ?>" /></div>

		<?php endwhile; ?>
		<?php wp_reset_postdata() . wp_reset_query(); ?>
		
	</div>
	
<?php endif; ?>