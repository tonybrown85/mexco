<?php
/**
 * The template for displaying search form
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

do_action( 'foundationpress_before_searchform' ); ?>

<?php if(Mage::getSingleton('customer/session')->isLoggedIn()): ?>

<div class="header-search">
<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	<div class="header-search-container">
	<div class="row">
		<?php do_action( 'foundationpress_searchform_top' ); ?>		
		
		<div class="small-12 medium-4 columns">
			
			<div class="header-special-offers">
				<span class="ionToggleButton"><i class="fa fa-bars"></i>Special Offers</span>
			</div>
			
		</div>
		
		<div id="the-search-container" class="small-12 medium-6 large-4 columns">
			<div id="header-search-wrapper">
				<div>
					<input type="text" value="" name="s" id="s" placeholder="<?php esc_attr_e( 'Search', 'foundationpress' ); ?>">
				</div>
				<?php do_action( 'foundationpress_searchform_before_search_button' ); ?>
				<div class="header-search-submit">
					<input type="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'foundationpress' ); ?>">
					<span class="fa fa-search"></span>
				</div>
			</div>
		</div>
		<div id="the-header-cart" class="small-12 medium-4 columns">
			<?php the_block('minicart_head'); ?>
		</div>
		<?php do_action( 'foundationpress_searchform_after_search_button' ); ?>
	</div>
	</div>
</form>
</div>

<?php else: ?>

<div class="header-search">
<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	<div class="header-search-container">
	<div class="row">
		<?php do_action( 'foundationpress_searchform_top' ); ?>		
		
		<div id="the-search-container" class="small-12 medium-4 small-centered columns">
			<div id="header-search-wrapper">
				<div>
					<input type="text" value="" name="s" id="s" placeholder="<?php esc_attr_e( 'Search', 'foundationpress' ); ?>">
				</div>
				<?php do_action( 'foundationpress_searchform_before_search_button' ); ?>
				<div class="header-search-submit">
					<input type="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'foundationpress' ); ?>">
					<span class="fa fa-search"></span>
				</div>
			</div>
		</div>
	</div>
	</div>
</form>
</div>

<?php endif; ?>

<?php do_action( 'foundationpress_after_searchform' ); ?>