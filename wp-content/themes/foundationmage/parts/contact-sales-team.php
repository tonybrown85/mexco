<?php

$args = array(
	'post_type' => 'mexco-team',
	'posts_per_page' => 12,
	'orderby' => 'date',
	'order'   => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'sectors',
			'field' => 'term_id',
			'terms' => array(10),
		)
	)
);

?>

<?php $loop = new WP_Query( $args ); ?>

<div id="team-members-row" class="row">

<div class="small-12 columns">
	<hr>
	<h2>Sales and Account Representatives</h2>
</div>

<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

	<?php $jobPosition = get_field('job_position'); ?>
	<?php $memberNumber = get_field('member_number'); ?>
	<?php $memberExtension = get_field('extension'); ?>
	<?php $memberEmail = get_field('member_email'); ?>

	<div class="small-12 medium-6 large-4 columns">
	
		<div class="sales-team-member">

			<header>
				<h4><?php echo get_the_title(); ?></h4>
				<?php if(!empty($jobPosition)): ?><h5><?php echo $jobPosition; ?></h5><?php endif; ?>
				<hr>
			</header>

			<?php if(!empty($memberNumber)): ?><p>Number: <?php echo $memberNumber; ?></p><?php endif; ?>
			<?php if(!empty($memberExtension)): ?><p>Ext No: <?php echo $memberExtension; ?></p><?php endif; ?>
			<?php if(!empty($memberEmail)): ?><p>Email: <a class="team-member-email" href="mailto:<?php echo $memberEmail; ?>"><?php echo $memberEmail; ?></a></p><?php endif; ?>

		</div>
		
	</div>

<?php endwhile; ?>

</div>