<?php
/**
 * Template part for top bar menu
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<div class="top-bar-container contain-to-grid">
    <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area top-bar-<?php echo apply_filters( 'filter_mobile_nav_position', 'mobile_nav_position' ); ?>">
            <li class="name">
                <h1><a href="<?php echo home_url(); ?>"><img class="site-logo" src="<?php the_field('site_logo', 'option'); ?>" alt="<?php bloginfo( 'name' ); ?>" /></a></h1>
            </li>
            <li class="toggle-topbar menu-icon"><a href="#">MENU<span></span></a></li>
        </ul>
        <section class="top-bar-section">
            <?php foundationpress_top_bar_l(); ?>
			<?php the_block('top.menu'); ?>
            <?php foundationpress_top_bar_r(); ?>
        </section>
    </nav>
</div>