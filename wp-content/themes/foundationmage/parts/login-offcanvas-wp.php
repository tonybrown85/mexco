<div class="off-canvas-wrap" data-offcanvas>
  <div class="inner-wrap">

    <!-- Off Canvas Menu -->
    <aside class="right-off-canvas-menu">
	
	<?php if(Mage::getSingleton('customer/session')->isLoggedIn()): ?>
	
	<?php $customer = Mage::getSingleton('customer/session')->getCustomer(); ?>
	
		<div class="right-off-canvas-menu-inner">

			<a class="exit-off-canvas"><span class="ion-ios-close-empty"></span></a>
			
			<div id="slide-account-header">
				<a name="account-login-modal"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/account-information.png" alt="Mexco Account Login" /></a>
				<a href="<?php echo Mage::getUrl('customer/account/logout') ?>"><span>Logout</span></a>
			</div>
			
			<div id="slide-account-content">
				<h5>Account Management Tools</h5>
				<p><?php echo $customer->getEmail(); ?></p>
				<?php echo foundationpress_stockist_account_menu(); ?>
			</div>
			
			<div id="slide-account-footer">
				<?php echo do_shortcode('[talk-to-us]'); ?>
			</div>
			
		</div>

	<?php else: ?>
		
		<div class="right-off-canvas-menu-inner">

			<a class="exit-off-canvas"><span class="ion-ios-close-empty"></span></a>
			
			<div id="slide-account-header">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/account-information.png" alt="Mexco Account Login" />
				<span>Login or create an Account</span>
			</div>
			
			<div id="slide-account-content">
				<h5>Registered Dealers</h5>
				<p>If you have an account, please log in</p>
				<?php the_block('offcanvas.login'); ?>
				<span class="canvas-password-forgot">Forgot Your Password?</span>
				<?php the_block('offcanvas.forgot'); ?>
				<div id="slide-account-content-bottom">
					<h5>New Dealers</h5>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at dolor diam. Donec ipsum diam, vehicula vel facilisis in, convallis sed nisi. Curabitur ultrices venenatis.</p>
					<a href="<?php echo get_site_url(); ?>/become-a-stockist/" class="button expand">Become A Stockist</a>
				</div>
			</div>
			
			<div id="slide-account-footer">
				<?php echo do_shortcode('[talk-to-us]'); ?>
			</div>
			
		</div>
		
	<?php endif; ?>
		
    </aside>