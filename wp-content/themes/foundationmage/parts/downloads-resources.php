<?php $downloadItems = get_field('download_items'); ?>

<?php if(Mage::getSingleton('customer/session')->isLoggedIn()): ?>

	<?php if(!empty($downloadItems)): ?>
	
	<div id="the-download-items" class="row">

		<?php foreach($downloadItems as $downloadItem): ?>

			<div class="small-12 medium-6 large-3 columns">

				<?php if($downloadItem['type'] == 'internal'): ?>

					<a class="button tiny expand" href="<?php echo $downloadItem['int_link']; ?>"><?php echo $downloadItem['item_label']; ?></a>

				<?php elseif($downloadItem['type'] == 'external'): ?>
				
					<a class="button tiny expand" href="<?php echo $downloadItem['ext_link']; ?>"><?php echo $downloadItem['item_label']; ?></a>

				<?php elseif($downloadItem['type'] == 'item'): ?>
				
					<a class="button tiny expand" href="<?php echo $downloadItem['file']; ?>" target="_blank" download="<?php echo $downloadItem['item_label']; ?>"><?php echo $downloadItem['item_label']; ?></a>

				<?php endif; ?>

			</div>

			<div class="small-12 medium-6 large-9 columns">
			
				<?php echo $downloadItem['content']; ?>

			</div>

		<?php endforeach; ?>
		
	</div>

	<?php endif; ?>
	
<?php else: ?>

	<h3>Please login to View Content</h3>

<?php endif; ?>