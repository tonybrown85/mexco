<?php $homeSlideshow = get_field('home_slideshow', 'option'); ?>

<?php if(!empty($homeSlideshow)): ?>

	<div class="homepage-slider-container">
	
		<div class="row">

			<div class="homepage-slider">

				<?php foreach($homeSlideshow as $slide): ?>
				
					<?php if($slide['slide_type'] == 'video'): ?>
					
						<?php if( isMobile() ): ?>

							<div class="home-slide"><img src="<?php echo $slide['gif_embed']['url']; ?>"<?php if(!empty($slide['gif_embed']['alt'])): ?> alt="<?php echo $slide['gif_embed']['alt']; ?>"<?php endif; ?> /></div>
						
						<?php else: ?>
						
							<div class="home-slide">
								<video controls autoplay loop>
									<?php if(!empty($slide['video_embed_mp4'])): ?><source src="<?php echo $slide['video_embed_mp4']; ?>" type="video/mp4"><?php endif; ?>
									<?php if(!empty($slide['video_embed_ogg'])): ?><source src="<?php echo $slide['video_embed_ogg']; ?>" type="video/ogg"><?php endif; ?>
									<?php if(!empty($slide['video_embed_webm'])): ?><source src="<?php echo $slide['video_embed_webm']; ?>" type="video/webm"><?php endif; ?>
									Your browser does not support the video tag.
								</video>
							</div>
						
						<?php endif; ?>
					
					<?php elseif($slide['slide_type'] == 'image'): ?>
					
						<div class="home-slide"><img src="<?php echo $slide['image']['sizes']['slideshow']; ?>"<?php if(!empty($slide['image']['alt'])): ?> alt="<?php echo $slide['image']['alt']; ?>"<?php endif; ?> /></div>
					
					<?php endif; ?>

				<?php endforeach; ?>
	
			</div>
		
		</div>
		
	</div>

<?php endif; ?>