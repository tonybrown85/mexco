<?php if(get_field('enable_product_categories', 'option')): ?>
	<?php
	$_helper = Mage::helper('catalog/category');
	$_categories = $_helper->getStoreCategories(false, true, false);
	$_categories->addAttributeToSelect('image');
	?>
	<?php $categoryCounter = 0; ?>
	<div class="clearfix"></div>
	<?php if (count($_categories) > 0): ?>
	<div class="row">
		<div id="home-product-categories">
			<div id="home-product-categories-inner">
					<?php foreach($_categories as $_category): ?>
						<?php if($_category->getName() !== 'Miscellaneous'): ?>
							<?php $categoryCounter++; ?>
								<div id="the-category-item-<?php echo $categoryCounter; ?>" class="the-category-item small-12 medium-4 columns">
									<a href="<?php echo $_helper->getCategoryUrl($_category); ?>">
										<div class="home-category-item">
												<h3><span><?php echo $_category->getName(); ?></span></h3>
												<?php if($_category->getimageurl()): ?><span class="the-category-item-image"><img src="<?php echo $_category->getimageurl(); ?>" /></span><?php endif; ?>
										</div>
									</a>
								</div>
								<?php if($categoryCounter == 3): ?><?php $categoryCounter = 0; ?><?php endif; ?>
						<?php endif; ?>
					<?php endforeach; ?>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<?php endif; ?>
<?php endif; ?>