<?php $enableCalls = get_field('enable_calls_for_action', 'option'); ?>
<?php $quickOffers = get_field('quick_offers_call_for_action', 'option'); ?>
<?php $downloadCall = get_field('download_call_for_action', 'option'); ?>
<?php $standardCall = get_field('standard_call_for_action', 'option'); ?>

<?php if(!empty($enableCalls)): ?>

	<div id="homepage-calls-for-action">
	
		<div class="row">
		
			<div id="home-calls-container" class="row">
			
				<div class="small-12 large-6 columns">
				
					<div class="row">

						<?php if(!empty($quickOffers)): ?>

							<?php foreach($quickOffers as $quickOffer): ?>
								
								<div id="cfa-quick-offers" class="small-12 columns">
								
									<a href="<?php echo $quickOffer['quick_offer_link']; ?>">
										<img src="<?php echo $quickOffer['quick_offer_image']['sizes']['large']; ?>"<?php if($quickOffer['alt']): ?> alt="<?php echo $quickOffer['alt']; ?>"<?php endif; ?> />
									</a>
								
								</div>
							
							<?php endforeach; ?>

						<?php endif; ?>
						
						<?php if(!empty($downloadCall)): ?>

							<?php foreach($downloadCall as $downloadCallItem): ?>
							
								<div id="cfa-downloads" class="small-12 medium-6 columns small-home-calls">
									
									<a href="<?php echo $downloadCallItem['download_call_file']; ?>" target="_blank" download="Download">
										<img src="<?php echo $downloadCallItem['download_call_image']['sizes']['large']; ?>"<?php if($downloadCallItem['download_call_image']['alt']): ?> alt="<?php echo $downloadCallItem['download_call_image']['alt']; ?>"<?php endif; ?> />
										<h3><?php echo $downloadCallItem['download_call_title']; ?><span data-tags="arrow, right" data-pack="default" class="ion-chevron-right"></span></h3>
									</a>
								
								</div>
							
							<?php endforeach; ?>

						<?php endif; ?>
						
						<?php if(!empty($standardCall)): ?>

							<?php foreach($standardCall as $standardCallItem): ?>
							
								<div id="cfa-standard" class="small-12 medium-6 columns small-home-calls">
								
									<a href="<?php echo $standardCallItem['standard_call_link']; ?>">
										<img src="<?php echo $standardCallItem['standard_call_image']['sizes']['large']; ?>"<?php if($standardCallItem['standard_call_image']['alt']): ?> alt="<?php echo $standardCallItem['standard_call_image']['alt']; ?>"<?php endif; ?> />
										<h3><?php echo $standardCallItem['standard_call_title']; ?><span data-tags="arrow, right" data-pack="default" class="ion-chevron-right"></span></h3>
									</a>
								
								</div>
							
							<?php endforeach; ?>

						<?php endif; ?>
						
					</div>
				
				</div>
				
				<div class="small-12 large-6 columns">
				
				<?php if(Mage::getSingleton('customer/session')->isLoggedIn()): ?>
				
					<?php
					
					$args = array(
						'post_type' => 'special-offer',
						'posts_per_page' => 24,
						'tax_query' => array(
							array(
								'taxonomy' => 'areas',
								'field' => 'term_id',
								'terms' => array(11),
							)
						)
					);
					
					?>
					<?php $loop = new WP_Query( $args ); ?>
					
					<div class="homepage-offers-container">
					
						<div class="homepage-offers-slider">
						
							<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
							
							<?php $offerThumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large-slider-banner' ); ?>
							<?php $offerThumbUrl = $offerThumb['0']; ?>
							
								<div><img src="<?php echo $offerThumbUrl; ?>" /></div>

							<?php endwhile; ?>
							
							<?php wp_reset_postdata() . wp_reset_query(); ?>
							
						</div>
						
						<h3>Special Offers<span data-tags="arrow, right" data-pack="default" class="ion-chevron-right"></span></h3>
					
					</div>
				
				<?php else: ?>
				
					<?php
					
					$args = array(
						'post_type' => 'special-offer',
						'posts_per_page' => 24,
						'tax_query' => array(
							array(
								'taxonomy' => 'areas',
								'field' => 'term_id',
								'terms' => array(8),
							)
						)
					);
					
					?>
					<?php $loop = new WP_Query( $args ); ?>
					
					<div class="homepage-offers-container">
					
						<div class="homepage-offers-slider">	
							
								<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
								
								<?php $offerThumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large-slider-banner' ); ?>
								<?php $offerThumbUrl = $offerThumb['0']; ?>
								
									<div><a class="login-to-top"><img src="<?php echo $offerThumbUrl; ?>" /></a></div>

								<?php endwhile; ?>
								
								<?php wp_reset_postdata() . wp_reset_query(); ?>
							
						</div>
						
						<h3><a class="login-to-top">Login for the Latest Offers<span data-tags="arrow, right" data-pack="default" class="ion-chevron-right"></span></a></h3>
					
					</div>
				
				<?php endif; ?>
					
				</div>
			
			</div>
		
		</div>
		
	</div>

<?php endif; ?>