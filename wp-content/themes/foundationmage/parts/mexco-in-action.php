<?php $actionItems = get_field('in_action_items'); ?>

<?php if(!empty($actionItems)): ?>

	<div id="in-action-items">

		<?php foreach($actionItems as $actionItem): ?>
		
			<div class="in-action-item">

				<h2><?php echo $actionItem['title']; ?></h2>
				<?php echo $actionItem['content']; ?>
			
			</div>

		<?php endforeach; ?>

	</div>

<?php endif; ?>