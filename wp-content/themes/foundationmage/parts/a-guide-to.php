<?php $guideItems = get_field('guide_items'); ?>

<?php if(!empty($guideItems)): ?>

	<div id="a-guide-to-items">

		<?php foreach($guideItems as $guideItem): ?>
		
		<div class="a-guide-to-item">
			
			<div class="row">
			
				<div class="small-12 medium-6 large-3 columns">
					<div class="guide-item-heading">
						<h2><?php echo $guideItem['title']; ?></h2>
						<a class="slide-link-small" href="<?php echo $guideItem['pdf_file']; ?>">Discover more</a>
					</div>
				</div>
				
				<div class="small-12 medium-6 large-8 columns right">
					<?php echo $guideItem['content']; ?>
				</div>	
			<div class="clearfix"></div>
			</div>
			
		</div>

		<?php endforeach; ?>

	</div>

<?php endif; ?>