<?php $appCharts = get_field('application_charts'); ?>

<?php if(!empty($appCharts)): ?>

	<?php foreach($appCharts as $appChart): ?>
	
		<div class="application-chart">
		
			<?php if(!empty($appChart['title'])): ?><h2><?php echo $appChart['title']; ?></h2><?php endif; ?>
			<?php if(!empty($appChart['content'])): ?><p><?php echo $appChart['content']; ?></p><?php endif; ?>
			<?php if(!empty($appChart['image'])): ?><img src="<?php echo $appChart['image']['url']; ?>" alt="<?php echo $appChart['title']; ?>" /><?php endif; ?>
			<?php if(!empty($appChart['pdf'])): ?><a class="slide-link-small" href="<?php echo $appChart['pdf']; ?>" title="<?php echo $appChart['title']; ?>" target="_blank">Discover more</a><?php endif; ?>
		
		</div>
	
	<?php endforeach; ?>

<?php endif; ?>