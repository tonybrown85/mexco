<div class="row">

	<div class="small-12 columns">

		<p>Copyright &copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?><span id="inspired-development"> | Web Development Cornwall, <a href="http://www.inspireddevelopment.co.uk">Inspired Development</a></span></p>
		<p>All rights reserved. All trademarks acknowledged.</p>

	</div>
	
</div>