<?php

$contactDetails = get_field('contact_details', 'option');
$contactFax = get_field('fax_number', 'option');
$contactFaxLabel = get_field('fax_number_label', 'option');
$contactAddress = get_field('business_address', 'option');

?>

<?php if(!empty($contactDetails)): ?>

	<div class="contact-page-details">

	<?php foreach($contactDetails as $contactItem): ?>

		<?php
		$icon = $contactItem['contact_details_icon'];
		$label = $contactItem['contact_details_label'];
		$link = $contactItem['contact_details_link'];
		?>

		<?php if($label == '0845 602 4274'): ?>

		<?php endif; ?>

		<?php if(!empty($link)): ?>

			<p><?php if($label == '0845 602 4274'): ?><span class="empt-item">Telephone: <?php elseif($label == 'email us: sales@mexco.co.uk'): ?><span class="empt-item">Email: </span><?php endif; ?><a href="<?php echo $link; ?>"><?php echo $icon; ?><?php if($label == 'email us: sales@mexco.co.uk'): ?>sales@mexco.co.uk<?php else: ?><?php echo $label; ?><?php endif; ?></a><?php if($label == '0845 602 4274'): ?></span><?php endif; ?></p>
			
			<?php if($label == '0845 602 4274' && !empty($contactAddress)): ?>
			
				<address><?php echo $contactAddress; ?></address>

			<?php endif; ?>
			
			<?php if($label == '0845 602 4274' && !empty($contactFax)): ?>
			
				<p class="contact-fax-item"><span class="empt-item">Fax: </span><a href="<?php echo $contactFax; ?>"><?php echo $contactFaxLabel; ?></a></p>

			<?php endif; ?>

		<?php else: ?>

			<p><span><?php echo $icon; ?><?php echo $label; ?></span></p>

		<?php endif; ?>

	<?php endforeach; ?>

	</div>

<?php endif; ?>