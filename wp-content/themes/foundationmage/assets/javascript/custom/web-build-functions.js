jQuery(document).ready(function($) {

	// Slick Sliders	
/*	
	$('.homepage-slider').slick({
	 autoplay:true,
	 fade:true,
	 slide:'.home-slide',
	 cssEase:'linear'
	});
	
	$('.homepage-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){

		$('.homepage-slider .slick-slide').find('video').get(0).pause();
	 
		var video = $('.homepage-slider .slick-active').find('video').get(0).play();
	 
	});
*/
	$('.homepage-offers-slider').slick({
		arrows : false,
		autoplay:true,
		dots : true
	});	
	
	$('#ion-product-category-slider').slick({
		arrows : false,
		dots : true
	});
	
	// Product Gallery Slider
	
	$('.product-gallery-slider').slick({
	  dots: false,
	  arrows: true,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  responsive: [
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
			infinite: true,
			dots: false
		  }
		},
		{
		  breakpoint: 600,
		  settings: {
			slidesToShow: 2,
			slidesToScroll: 2
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
	  ]
	});
	
	// Swipebox

	$('.swipebox').swipebox();
	
	// FAQ Accordion Reveal All
	
	// Animate Accordions
/*
	$(".faq-accordion li").on("click", "a:eq(0)", function (event)
      {
        var dd_parent = $(this).parent();

        if(dd_parent.hasClass('active'))
          $(".faq-accordion li div.content:visible").slideToggle("normal");
        else
        {
          $(".faq-accordion li div.content:visible").slideToggle("normal");
          $(this).parent().find(".content").slideToggle("normal");
        }
    });	
	*/
	if ($('.faq-group').length > 0) {
				
		
		$('.toggle-all-wrapper').prepend('<span class="faq-toggle faq-closed">Open All</span>');
	
		$(document).on('click','.accordion-item-title', function() {
			
			$('.faq-toggle.faq-opened').text('Show All');
			$('.faq-toggle.faq-opened').removeClass('faq-opened').addClass('faq-closed');
			
		});
		
		$(document).on('click','.faq-toggle', function() {
			
			$('.faq-group .accordion-navigation').removeClass('active');
			$('.faq-group .accordion-navigation .content').removeClass('active');
			
			if ($(this).hasClass('faq-closed')) {
				
				if ($('.faq-toggle.faq-opened').length > 0) {
					
					$('.faq-toggle.faq-opened').text('Show All');
					$('.faq-toggle.faq-opened').removeClass('faq-opened').addClass('faq-closed');
			
				}
				
				$(this).text('Close All');
				$(this).removeClass('faq-closed').addClass('faq-opened');
				
				$(this).closest('.faq-group').find('.accordion-navigation').addClass('active');
				$(this).closest('.faq-group').find('.content').addClass('active');		
			
			} else {
				
				$(this).text('Open All');
				$(this).removeClass('faq-opened').addClass('faq-closed');
				
			}		
			
		});	
		
	}
	
	// Toggles
	
	$( ".ionToggleButton" ).click(function() {
	  $( ".offers-header" ).slideToggle( "slow", function() {

	  });
	});
		
	$( ".canvas-password-forgot" ).click(function() {
	  $( ".forgot-reveal" ).slideToggle( "slow", function() {

	  });
	});
	
	// Slide To Top Modal - Fire Once
	
	$('.login-to-top').one('click', function() {
		setTimeout(function() {
		 $("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
		}, 500);
	});
	
    // =============================================
    // Skip Links
    // =============================================

    var skipContents = $('.skip-content');
    var skipLinks = $('.skip-link');

    skipLinks.on('click', function (e) {
        e.preventDefault();

        var self = $(this);
        // Use the data-target-element attribute, if it exists. Fall back to href.
        var target = self.attr('data-target-element') ? self.attr('data-target-element') : self.attr('href');

        // Get target element
        var elem = $(target);

        // Check if stub is open
        var isSkipContentOpen = elem.hasClass('skip-active') ? 1 : 0;

        // Hide all stubs
        skipLinks.removeClass('skip-active');
        skipContents.removeClass('skip-active');

        // Toggle stubs
        if (isSkipContentOpen) {
            self.removeClass('skip-active');
        } else {
            self.addClass('skip-active');
            elem.addClass('skip-active');
        }
    });

    $('#header-cart').on('click', '.skip-link-close', function(e) {
        var parent = $(this).parents('.skip-content');
        var link = parent.siblings('.skip-link');

        parent.removeClass('skip-active');
        link.removeClass('skip-active');

        e.preventDefault();
    });
	
	// Account Area
	
	$('.customer-account-page .my-account').wrap('<div class="row"></div>');
	$('.customer-account-page .page-title').wrap('<div class="small-12 columns"></div>');
	$('.customer-account-page .welcome-msg').wrap('<div class="small-12 columns"></div>');
	$('.customer-account-page .box-head').wrap('<div class="small-12 columns"></div>');
	$('.customer-account-page .box-account .col2-set').addClass('small-12 medium-6 columns');
	$('.customer-account-page .customer-name-middlename .field.name-firstname').addClass('small-12 medium-6 large-4 columns');
	$('.customer-account-page .customer-name-middlename .field.name-middlename').addClass('small-12 medium-6 large-4 columns');
	$('.customer-account-page .customer-name-middlename .field.name-lastname').addClass('small-12 medium-12 large-4 columns');
	$('.customer-account-page .form-list label[for=email]').addClass('small-12 medium-12 columns');
	$('.customer-account-page .form-list #email').wrap('<div class="small-12 medium-12 columns"></div>');
	$('.customer-account-page .control').addClass('small-12 medium-12 columns');
	$('.customer-account-page .buttons-set').addClass('small-12 medium-12 columns');
	$('.customer-account-page .legend').addClass('small-12 medium-12 columns');
	$('.customer-account-page .wide').addClass('small-12 medium-12 columns');
	$('.customer-account-page .form-list label[for=telephone]').addClass('small-12 medium-12 columns');
	$('.customer-account-page .form-list #telephone').wrap('<div class="small-12 medium-12 columns"></div>');
	$('.customer-account-page .form-list label[for=fax]').addClass('small-12 medium-12 columns');
	$('.customer-account-page .form-list #fax').wrap('<div class="small-12 medium-12 columns"></div>');
	$('.customer-account-page .form-list label[for=city]').addClass('small-12 medium-12 columns');
	$('.customer-account-page .form-list #city').wrap('<div class="small-12 medium-12 columns"></div>');
	$('.customer-account-page .form-list label[for=region_id]').addClass('small-12 medium-12 columns');
	$('.customer-account-page .form-list #region_id').wrap('<div class="small-12 medium-12 columns"></div>');
	$('.customer-account-page .form-list label[for=zip]').addClass('small-12 medium-12 columns');
	$('.customer-account-page .form-list #zip').wrap('<div class="small-12 medium-12 columns"></div>');
	$('.customer-account-page .form-list label[for=country]').addClass('small-12 medium-12 columns');
	$('.customer-account-page .form-list #country').wrap('<div class="small-12 medium-12 columns"></div>');
	$('.customer-account-page #wishlist-view-form').wrap('<div class="small-12 columns"></div>');
	
	// Find a Stockist
	
	$('#wpsl-search-wrap').addClass('small-12 columns');
	$('#wpsl-search-wrap').wrap('<div class="row"></div>');
	$('#wpsl-result-list').addClass('small-12 columns');
	$('#wpsl-result-list').wrap('<div class="row"></div>');
	
	// Merge the Search

	var $wps = 'http://127.0.0.1/mexco/?s=',
		$mgs = 'http://127.0.0.1/mexco/shop/catalogsearch/result/?q=',
		$wp_data = '', $mg_data = '', $box = '';
		
		$box += '<div id="header-search-wrapper">';
		$box += '<div><input type="text" placeholder="Search" id="s" name="s" value=""></div>';
		$box += '<div class="header-search-submit">';
		$box += '<input type="submit" value="Search" id="searchsubmit">';
		$box += '<span class="fa fa-search"></span>';
		$box += '</div></div>';
		
	$('#the-search-container').html($box);
				
	function searchHandler($request) {
			
		$('#header-search-wrapper, #magento-wordpress-search-results').addClass('reloading');
			
		var $wp_post = $wps + $request,
			$mg_post = $mgs + $request,
			$wp_state = 0, $mg_state = 0,
			$get_dom = '';
		
		$.get($wp_post, function(data) {
				
			$wp_state = 1;
			$wp_data = $($.parseHTML(data)).find("#search_result").html();			
			$wp_data = (typeof $wp_data != 'undefined' ? $.trim($wp_data) : '');
				
		});
		
		$.get($mg_post, function(data) {
				
			$mg_state = 1;
			$mg_data = $($.parseHTML(data)).find("#search_result .category-products").html();			
			$mg_data = (typeof $mg_data != 'undefined' ? $.trim($mg_data) : '');
				
		});		
			
				
				
			
		$(document).ajaxComplete(function() {
				
			if ($wp_state !== 0 && $mg_state !== 0) {
				
				$get_dom = $('<div id="sort">' + $wp_data + $mg_data + '</div>');	
							
				$('#magento-wordpress-search-results').html('');

				$get_dom.find('.resulter').sort(function (a, b) {
					
				   return $(a).attr('data-name') > $(b).attr('data-name');
				   
				}).appendTo('#magento-wordpress-search-results');
				
				var $resultTitle = ($wp_data.length + $mg_data.length !== 0 ? 'Search Results' : 'No Results Found');
				
				$('#magento-wordpress-search-results').prepend('<div class="row"><div class="small-12 columns"><h3 id="wpmage-search-result-title">' + $resultTitle + '</h3></div></div>');
			
				$('#header-search-wrapper, #magento-wordpress-search-results').removeClass('reloading');
			
			}
				
		});
		
	}
	
	$(document).on('click', '#searchsubmit', function(e) {
		
		e.preventDefault();
		
		var $request = $('#s').val(),
			$result = '';
		
		if ($request.length > 0) { $result = searchHandler($request); }
		
	});
	
	// WP Store Locator - Extended
	
	if ($('#wpsl-search-input').length > 0) {
	
		function getTresults() {
					
				$("#wpsl-new-result").remove();
				
				var $resultStatus = ($('#wpsl-stores ul li:first-child').hasClass('no-results') ? 1 : 0); 
						
				var $theStoreCounter = ($resultStatus == 1 ? '0' : $('#wpsl-stores ul li').length),
					$theStoreValue   = $('#wpsl-search-input').val(),
					$theStoreValue   = ($theStoreValue !== '' ? 'for ' +  $theStoreValue : ''),
					$tableTop        = '<div id="stockist-table-top"><div class="wpsl-store-location"><p>Stockist</p></div><div class="wpsl-direction-wrap"><p>Directions</p></div><div class="clearfix"></div></div>',
					$updateString    = "Your search found " + $theStoreCounter + " results " + $theStoreValue + $tableTop,
					$pushString      = '';
			
			if ($theStoreCounter == '0') {
				
					$pushString  = '<h3 id="wpsl-new-result">' + $updateString + '</h3>';
					$pushString += '<p id="wpsl-nostockist1">There are no Mexco Stockists within 15 miles of Your Location</p>';
					$pushString += '<p id="wpsl-nostockist2">You can <a id="wpslns-link1" href="contact-us">contact us</a> via email or call <a id="wpslns-link2" href="tel:08456024274">0845 602 4274</a> and we will advise you of your nearest Mexco dealer.</p>';
						
				
			} else {
				
					$pushString = '<h3 id="wpsl-new-result">' + $updateString + '</h3>';
						
			}
				
			$('#wpsl-stores').prepend($pushString);
			
		}	
		
		$(document).ajaxComplete(function() { getTresults(); });
	
	}
	
	// Parallax
	
	speed = 0.5;

	window.onscroll = function(){

		if($(window).width() > 768){

			$(".parallax").each(function(index, value) {

				var containerOffset = $(this).offset().top,  
					windowTop = $(window).scrollTop(),
					scrollAmount = (windowTop - containerOffset - 800);
				
				$(this).css('background-position', "center " + (scrollAmount * speed) + "px")

			});

		}
	
	}
	
	// Funk the Offcanvas

	$(document).on('click', '.toggle-the-canvas', function() {
	
		$('.right-off-canvas-menu').toggleClass('overlay-the-canvas');
		
	});

	$(document).on('click', '.exit-off-canvas .ion-ios-close-empty', function() {
	
		$('.right-off-canvas-menu').toggleClass('overlay-the-canvas');
		
	});

	$(document).on('click', '.login-to-top', function() {
	
		$('.right-off-canvas-menu').toggleClass('overlay-the-canvas');
		
	});
	
	// Open the Canvas on Contact page
	/*
	if ($('body').hasClass('page-template-page-contact')) {
	
		$('.right-off-canvas-menu').addClass('overlay-the-canvas');
	
	}
	*/
	
	// Move The Top Level Menu
	$tempClass = '', $tempClassSecond = '';
	
	
	function hotspotReset() {
		
		$('.menu-hotspot').slideUp(500, function() {
			
			$('*').removeClass('push-menu tri-menu-child push-menu-second product-category-child');
		
			$('.menu-hotspot').removeClass($tempClass);
			
			$('.product-menu-hotspot').removeClass($tempClassSecond);
				
			$('.menu-hotspot > .row, .product-menu-hotspot').html('');
			
			$toggledChild = 0;
			
		});
			
		
	}
	
		$(document).on('click', '.top-bar .menu-item-has-children', function() {
				
			if (windowWidth > 1280) { // here - only allows triggering of the menu if the current width is below
				
			if ($(this).hasClass('push-menu')) {
				
				hotspotReset();
				
			} else {
				
				hotspotReset();	
				
				var $getThis = $(this),
					$getDowns = $getThis.find('.sub-menu').clone(),				
					$getClass = $getThis.children('a').text().toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');
				
				setTimeout(function(){
						
					$getThis.addClass('push-menu');						
					
					$('.menu-hotspot > .row').html($getDowns);
					
					$('.menu-hotspot').slideDown();
							
					$('.menu-hotspot').removeClass($tempClass).addClass($getClass);
						
					$tempClass = $getClass;				
						
					if ($getThis.hasClass('tri-menu-parent')) {
						
						$('.menu-hotspot > .row').addClass('tri-menu-child');
							
					}
				
				}, 750);
			
			}
			
			}
			
		});
		
		// Move The 2nd Level Menu	
		
		$toggledChild = 0;
		
		$(document).on('mouseover', '.product-category-has-child > a', function(e) {
				
			if (windowWidth > 1280) { // here - only allows triggering of the menu if the current width is below
			
			e.preventDefault(); // only prevent default click event if we are below the desired width, else we want it to behave as normal
				
			if (!$(this).closest('li').hasClass('push-menu-second')) {
				
				$('.push-menu-second, .product-category-child').removeClass('push-menu-second product-category-child');	
				
				var $getThis = $(this),
					$getDownsSecond = $getThis.closest('.product-category-has-child').find('.category-menu-info').clone(),				
					$getClassSecond = $getThis.children('span').text().toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');
				
				$getThis.closest('li').addClass('push-menu-second');
										
					
				if ($toggledChild == 0) {
					
					$('.product-menu-hotspot').html($getDownsSecond).hide();		
						
				} else {
					
					$('.product-menu-hotspot').fadeOut(500,function() {						
						
						var $tempHeight = $('.product-menu-hotspot').height();
						
						$('.product-menu-hotspot').css('height', $tempHeight);	
						
						$('.product-menu-hotspot').html($getDownsSecond);
						
						$('.product-menu-hotspot').attr('style', '');
						
					});
						
				}
					
				
				setTimeout(function(){		
					
					if ($toggledChild == 0) {
						
						$('.product-menu-hotspot').slideDown();
						
					} else {
						
						$('.product-menu-hotspot').fadeIn();
						
					}
					
					console.log($toggledChild);
						
					$('.product-menu-hotspot').removeClass($tempClassSecond).addClass($getClassSecond);
					
					$tempClassSecond = $getClassSecond;
					
					if ($getThis.closest('li').hasClass('product-category-reveal')) {
						
						$('.product-menu-hotspot').addClass('product-category-child');
						
					}
					
					$toggledChild = 1;
					
				}, 750);
				
				
			}
				
			
			}
			
		});
		

	
	// Functions for Window Size
	function windowSize() {
		
		windowHeight = window.innerHeight ? window.innerHeight : $(window).height();
		windowWidth = window.innerWidth ? window.innerWidth : $(window).width();		
		
		if (windowWidth < 1280) { hotspotReset(); } // here - this rule is the other way around, because if the page resizes below the desired level we want it to clear any menu classes and empty any hotspot divs - effectively resetting the menu
		
	}
	
	windowSize();
	
	$(window).resize(function() { windowSize(); });
	
	
	// Social Share Slide Toggle on Product Page
	
	$('.share-the-product-page').on('click', function() {
	  $( ".share-the-product-page-container" ).slideToggle( "slow", function() {
	  });
	});
	
	if ($('#google-map').length > 0) {
	$.loadMap();
	}
});