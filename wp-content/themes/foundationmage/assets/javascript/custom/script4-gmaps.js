// MAPS
jQuery.loadMap = function() {
	
	function addMaps() {
		
		var mapscript=document.createElement('SCRIPT');
			mapscript.type='text/javascript';
			mapscript.src="http://maps.googleapis.com/maps/api/js?callback=loadMaps";
			document.getElementsByTagName("head")[0].appendChild(mapscript);
		
	}
 
	addMaps();

	window.loadMaps = function() {

		var $pinImage = new google.maps.MarkerImage("http://127.0.0.1/mexco/wp-content/themes/foundationmage/assets/images/icons/map-marker.png",
		new google.maps.Size(50, 60),
		new google.maps.Point(0,0),
		new google.maps.Point(21, 60)),
		$map_block = jQuery('#google-map'),
		$map_height = (400), //$map_block.width() / 2
		$map_tit = $map_block.attr('data-title'),
		$map_add = $map_block.attr('data-address'),
		$map_lat = $map_block.attr('data-lat'),
		$map_lng = $map_block.attr('data-lng'),
		$map_content = '<p><b>' + $map_tit + '</b><br />' + $map_add.replace(/,/g, "<br />") + '</p>',
		$map_info = new google.maps.InfoWindow({ content: $map_content });
		
		// </div>
				
		var styles = [{
			"featureType": "administrative",
			"stylers": [{
				"weight": 0.3
			},
			{
				"color": "#666666"
			}
				      ]
		},
		{
			"featureType": "landscape",
			"stylers": [{
				"color": "#E5E5E5"
			}
				      ]
		},
		{
			"featureType": "poi",
			"stylers": [{
				"color": "#999999"
			},
			{
				"visibility": "off"
			}
				      ]
		},
		{
			"featureType": "road",
			"stylers": [{
				"color": "#999999"
			},
			{
				"weight": 0.2
			},
			{
				"visibility": "on"
			}
				      ]
		},
		{
			"featureType": "road",
			"elementType": "labels",
			"stylers": [{
				"visibility": "off"
			}
				      ]
		},
		{
			"featureType": "water",
			"stylers": [{
				"weight": 0.1
			},
			{
				"color": "#F0F0F0"
			}
				      ]
		},
		{
			"featureType": "transit",
			"stylers": [{
				"visibility": "off"
			}
				      ]
		}
			  ];
		  
		var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});


		
		$map_block.css('height', $map_height);

		$build = {
			center: new google.maps.LatLng($map_lat,$map_lng),
			zoom: 14,
			scrollwheel: false,			
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
			}
		};	
		

		$map_build = new google.maps.Map(document.getElementById('google-map'), $build);
	
		$map_build.mapTypes.set('map_style', styledMap);
		$map_build.setMapTypeId('map_style');

		$marker_build = new google.maps.Marker({
			position: $build.center,
			map: $map_build,
			icon: $pinImage
		});
		
		google.maps.event.addListener($marker_build, 'click', function() {
			$map_info.open($map_build,$marker_build);
			$map_build.panBy(0, -100);
		});

	}
	
};
