<?php

// Remove Dashboard

function dashboard_redirect() {
	
    wp_redirect(admin_url('edit.php?post_type=page'));
	
}

add_action('load-index.php','dashboard_redirect');

function removeDashboard() {

    remove_menu_page( 'index.php' );

}
   
add_action( 'admin_menu', 'removeDashboard', 99 );

// ACF Option Pages

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Settings',
		'menu_title'	=> 'Mexco',
		'menu_slug' 	=> 'theme-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}

// Cookie Notice

function theCookieNotice() { ?>

<?php if ( is_front_page() || is_home() ): ?>

	<div id="the-cookie-notice" data-alert class="alert-box secondary">
		<div class="cookie-row">
		  <p>This site uses cookies to improve your experience. By using our website you accept our <a class="cookie-privacy-link" href="<?php echo get_site_url(); ?>/legal-privacy/">Privacy Policy</a> and use of cookies. <a href="#" class="close slide-link"><span>Accept</span></a> <a class="notice-read-more" href="<?php echo get_site_url(); ?>/legal-privacy/">Learn More</a></p>
		</div>
	</div>
	
<?php endif; ?>

<?php }

add_action('foundationpress_after_footer', 'theCookieNotice');

// Post Types

function create_post_type() {
	
	register_post_type('special-offer',
		array(
			'labels' => array(
			'name' => __( 'Special Offers' ),
			'singular_name' => __( 'Offer' )
			),
		'public' => true,
		'show_ui' => true,
		'has_archive' => false,
		'menu_icon' => 'dashicons-megaphone',
		'supports' => array('title','thumbnail'),
		'menu_position' => -5
		)
	);
	/*
	register_post_type('product-info',
		array(
			'labels' => array(
			'name' => __( 'Product Information' ),
			'singular_name' => __( 'Info Item' )
			),
		'public' => true,
		'show_ui' => true,
		'has_archive' => false,
		'menu_icon' => 'dashicons-info',
		'supports' => array('title','thumbnail'),
		'menu_position' => -4
		)
	);

	register_post_type('resource',
		array(
			'labels' => array(
			'name' => __( 'Demand The Best' ),
			'singular_name' => __( 'Resource' )
			),
		'public' => true,
		'show_ui' => true,
		'has_archive' => false,
		'menu_icon' => 'dashicons-awards',
		'supports' => array('title','thumbnail'),
		'menu_position' => -3
		)
	);
	
	register_post_type('download',
		array(
			'labels' => array(
			'name' => __( 'Downloads' ),
			'singular_name' => __( 'Download' )
			),
		'public' => true,
		'show_ui' => true,
		'has_archive' => false,
		'menu_icon' => 'dashicons-download',
		'supports' => array('title','thumbnail'),
		'menu_position' => -2
		)
	);
	*/

	register_post_type('mexco-team',
		array(
			'labels' => array(
			'name' => __( 'The Team' ),
			'singular_name' => __( 'Staff Member' )
			),
		'public' => true,
		'show_ui' => true,
		'has_archive' => false,
		'menu_icon' => 'dashicons-smiley',
		'supports' => array('title'),
		'menu_position' => -1
		)
	);
	
	flush_rewrite_rules();

}
add_action( 'init', 'create_post_type' );

// Taxonomies

function create_taxonomies() {
	register_taxonomy(
		'areas',
		'special-offer',
		array(
			'label' => __( 'Offer Areas' ),
			'rewrite' => array( 'slug' => 'areas' ),
			'hierarchical' => true,
		)
	);
	register_taxonomy(
		'sectors',
		'mexco-team',
		array(
			'label' => __( 'Sectors' ),
			'rewrite' => array( 'slug' => 'sectors' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'create_taxonomies' );

// SVG Support in WP Admin

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

// Off Canvas Login

function loginOffcanvas() {
	if(is_page() || is_single() || is_search() || is_category()) {
		get_template_part('parts/login-offcanvas-wp');
	}
	
}

add_action('foundationpress_after_body', 'loginOffcanvas');

function loginCanvasClose() {

echo '</div></div>';
	
}

add_action('foundationpress_before_closing_body', 'loginCanvasClose');

// Inject Div for Merged Search

function globalMageWPSearch() {

	echo '<div id="magento-wordpress-search-results"></div>';
	
}

add_action('foundationpress_after_offers', 'globalMageWPSearch');

// Mobile Detection

function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

add_action('init', 'isMobile');

// Menu Hotspot

function menuHotspot() {
	
	echo '<div class="menu-hotspot"><div class="category-menu-info-bg"></div><div class="row"></div><div class="clearfix"></div><div class="product-menu-hotspot"><div class="clearfix"></div></div></div>';
	
}

add_action('foundationpress_after_header', 'menuHotspot');

?>