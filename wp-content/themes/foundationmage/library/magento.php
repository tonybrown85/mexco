<?php

// Remove WP Meta Title from WP Head when on Magento Pages

function removeWpMetaTitle() {
	remove_action( 'wp_head','_wp_render_title_tag', 1 );
}

// Inject Magento Menu into WP Primary Menu & Mobile Menu

function magentoMenu( $nav, $args ) {
	if( $args->theme_location == 'top-bar-l' )
		
		if(is_page() || is_single() || is_search() || is_category()) {
			
			return the_block('top.menu') . $nav;
			
		} else {
			
			return $nav;
			
		}
		return $nav;
}

// add_filter('wp_nav_menu_items','magentoMenu', 10, 2);

// Current Path

$GLOBALS['getPath'] = $_SERVER['DOCUMENT_ROOT'].$_SERVER['REQUEST_URI'];