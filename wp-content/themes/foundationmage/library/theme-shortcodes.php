<?php

// Shortcode Support in text Widget

add_filter('widget_text', 'do_shortcode');

// Sidebar Navigation

function sidebarNav() {
 
$productCatEnable = get_field('product_categories_in_sidebar', 'option');
$productCatTitle = get_field('product_categories_title', 'option');
$sidebarNavigation = get_field('sidebar_nav', 'option');

$return = '';
 
ob_start(); 
 
?>

<div id="mexco-sidebar-nav">

<ul class="accordion" data-accordion>

<?php
if($productCatEnable) {

?>
   <li class="accordion-navigation">
   
    <a class="sidebar-nav-title" href="#panel1a"><?php echo $productCatTitle; ?></a>
    
    <div id="panel1a" class="content active">
		<?php get_template_part('parts/product-category-navigation'); ?>
    </div>
    
   </li>
   
<?php
}
?>

</ul>
 
<?php


if (!empty($sidebarNavigation)) {
  
 if(!empty($productCatEnable)) {
   
  $navCounter = 1;
   
 } else {
   
  $navCounter = 0; 
   
 }
  
 foreach($sidebarNavigation as $sidebarNavMenu) {
   
 ++$navCounter;

?>
   
<ul class="accordion" data-accordion>

 <li class="accordion-navigation">

  <a class="sidebar-nav-title" href="#panel<?php echo $navCounter; ?>a"><?php echo $sidebarNavMenu['title']; ?></a>

  <div id="panel<?php echo $navCounter; ?>a" class="content active">

   <ul>

<?php

foreach($sidebarNavMenu['nav'] as $navItems) {
 
 $sidebarNavLink = ($navItems['type'] == 'internal link' ? $navItems['internal'] : $navItems['external']);
 
?>
  
   <li><a href="<?php echo $sidebarNavLink; ?>"><?php echo $navItems['label']; ?></a></li>
  
  
<?php  } ?>
    
   </ul>
     
  </div>
  
 </li>
  
</ul>
     
<?php
 }  
}

 
$return = ob_get_clean(); 

return $return;
 
 
 /*
 $sidebarNav = '';
  
 $sidebarNav .= '<div id="mexco-sidebar-nav">';
 $sidebarNav .= '<ul class="accordion" data-accordion>';
 
 if($productCatEnable) {

  if(is_page() || is_single() || is_search() || is_category()) {
   
   $sidebarNav .= '<li class="accordion-navigation">';
   $sidebarNav .= '<a class="sidebar-nav-title" href="#panel1a">';
   $sidebarNav .= $productCatTitle;
   $sidebarNav .= '</a>';
   $sidebarNav .= '<div id="panel1a" class="content active">';
   $sidebarNav .= get_block('catalog.sidebar');
   $sidebarNav .= '</div></li>'; 
   
  }
  
 }
 
 $sidebarNav .= '</ul>';
 
 if(!empty($sidebarNavigation)) {
  
  if(!empty($productCatEnable)) {
   
   $navCounter = 1;
   
  } else {
   
   $navCounter = 0; 
   
  }
  
  foreach($sidebarNavigation as $sidebarNavMenu) {
   
   $navCounter++;
   
   $sidebarNav .= '<ul class="accordion" data-accordion>';
   $sidebarNav .= '<li class="accordion-navigation">';
   $sidebarNav .= '<a class="sidebar-nav-title" href="#panel' . $navCounter . 'a">';
   $sidebarNav .= $sidebarNavMenu['title'];
   $sidebarNav .= '</a>';
   $sidebarNav .= '<div id="panel' . $navCounter . 'a" class="content active">';
   $sidebarNav .= '<ul>';

    foreach($sidebarNavMenu['nav'] as $navItems) {
     
     $sidebarNav .= '<li>';
     $sidebarNav .= '<a href="';
     
     if($navItems['type'] == 'internal link') {
      
      $sidebarNav .= $navItems['internal'];
            
     } elseif($navItems['type'] == 'external link') {
      
      $sidebarNav .= $navItems['external'];     
     
     }
     
     $sidebarNav .= '">'; 
     $sidebarNav .= $navItems['label'];
     $sidebarnav .= '</a>';
     $sidebarnav .= '</li>'; 
     
    }
    
   $sidebarNav .= '</ul>';
   
   $sidebarNav .= '</div></li>';
   $sidebarNav .= '</ul>';
   
  }
  
 }
 
 return $sidebarNav; 
 
 */
 
}

add_shortcode('sidebar-navigation', 'sidebarNav');

// Speak to a Human - Sidebar

function talkToMe() {
	
$contactDeets = get_field('contact_details', 'option');

$talkToMe = '';

$talkToMe .= '<div id="talk-to-us">';
$talkToMe .= '<p>Want to talk to a Human<br />Give Us a Call';

if(!empty($contactDeets)) {
	
	foreach($contactDeets as $contactDeet) {
		
		$talkToMe .= '<a href="' . $contactDeet['contact_details_link'] . '">' . $contactDeet['contact_details_label'] . '</a>';
		break;
	}
	
}

$talkToMe .= '</p>';
$talkToMe .= '</div>';

return $talkToMe;
	
}

add_shortcode('talk-to-us', 'talkToMe');

function wholesaleRegister() {

	$registration = get_block('mexco.registration');

	return $registration;
	
}

add_shortcode('mexco-registration', 'wholesaleRegister');

?>