<?php
/*
Template Name: Contact Us
*/
get_header(); ?>
<?php $mapEmbed = get_field('map_embed'); ?>
<?php if(!empty($mapEmbed)): ?>
	<div class="map-embed">
	<div class="map-overlay" onclick="style.pointerEvents='none'"></div>
		<div id="google-map" data-title="Mexco" data-address="Carminow Rd, Bodmin" data-lat="50.4581064" data-lng="-4.7022857"></div>
	</div>
<?php endif; ?>
<div class="row">
	<div class="small-12 large-12 columns" role="main">

	<?php /* Start loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<div class="entry-content">
				<?php the_content(); ?>
				<?php $formShortcode = get_field('contact_form_shortcode'); ?>
				<?php if(!empty($formShortcode)): ?><?php echo do_shortcode($formShortcode); ?><?php endif; ?>
				<?php get_template_part('parts/contact-details'); ?>
				<?php get_template_part('parts/contact-sales-team'); ?>
			</div>
			<footer>
				<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
				<p><?php the_tags(); ?></p>
			</footer>
			<?php comments_template(); ?>
		</article>
	<?php endwhile; // End the loop ?>

	</div>
</div>

<?php get_footer(); ?>
