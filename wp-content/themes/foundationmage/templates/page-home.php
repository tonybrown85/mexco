<?php
/*
Template Name: Homepage
*/
get_header(); ?>

<div role="main">

<?php while ( have_posts() ) : the_post(); ?>

	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
	
		<?php get_template_part('parts/home/home-product-categories'); ?>
		<?php get_template_part('parts/home/home-calls'); ?>
		
		<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hd-image' ); ?>
		<?php $thumbUrl = $thumb['0']; ?>
		
		<div id="homepage-content" class="parallax" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/square-dot-overlay.png), url(<?php echo $thumbUrl; ?>);">
		
			<div class="row">
			
			<div class="small-12 large-5 columns right">

				<header class="entry-header">
				
					<h1 class="entry-title"><?php the_title(); ?></h1>
					
				</header>
				
				<div class="entry-content">
				
					<?php the_content(); ?>
					
				</div>
				
			</div>
				
			</div>
			
		</div>
		
	</article>
	
<?php endwhile; ?>

</div>

<?php get_footer(); ?>
