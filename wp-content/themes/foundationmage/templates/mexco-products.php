<?php
/*
Template Name: Mexco Products
*/
get_header(); ?>

<div id="content-row" class="row">
	<div id="row-stretch">
		<div class="small-12 large-9 large-push-3 columns" role="main">

			<?php do_action( 'foundationpress_before_content' ); ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
					<header>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header>
					<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
					<div class="entry-content">
						<?php // the_content(); ?>
						<?php get_template_part('parts/page-builder'); ?>
					</div>
					<footer>
						<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
						<p><?php the_tags(); ?></p>
					</footer>
				</article>
			<?php endwhile; ?>

			<?php do_action( 'foundationpress_after_content' ); ?>

		</div>
		<?php get_sidebar( 'left' ); ?>
	</div>
</div>
<?php get_footer(); ?>
