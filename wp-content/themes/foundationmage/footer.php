<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

</section>

<div id="newsletter-container">

	<?php echo the_block('the.newsletter'); ?>

</div>

<?php do_action( 'foundationpress_before_footer' ); ?>

<footer>

	<div id="footer-top-container">

		<?php get_template_part('parts/footer-top'); ?>
		
	</div>

	<div id="footer-mid-container">

		<?php get_template_part('parts/footer-mid'); ?>

	</div>

	<div id="footer-bottom-container">

		<?php get_template_part('parts/footer-bottom'); ?>

	</div>

</footer>

<?php do_action( 'foundationpress_after_footer' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
<a class="exit-off-canvas"></a>
<?php endif; ?>

	<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
	</div>
</div>
<?php endif; ?>

<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
