<?php
/*
Plugin Name: Mexco Stockist Sync
Plugin URI: http://www.inspireddevelopment.co.uk
Description: A plugin to Sync Mexco's stockists in Magento with WP Store Locator.
Author: Tony Brown (Inspired Development)
Author URI: http://tonybrown.eu
Version: 1.0
License: GPLv2
*/

if ( !function_exists( 'add_action' ) ) {
	echo "This page cannot be called directly.";
	exit;
}


if ( ! defined( 'WP_CONTENT_URL' ) ) {	
	if ( ! defined( 'WP_SITEURL' ) ) define( 'WP_SITEURL', get_option("siteurl") );
	define( 'WP_CONTENT_URL', WP_SITEURL . '/wp-content' );
}
if ( ! defined( 'WP_SITEURL' ) ) define( 'WP_SITEURL', get_option("siteurl") );
if ( ! defined( 'WP_CONTENT_DIR' ) ) define( 'WP_CONTENT_DIR', ABSPATH . 'wp-content' );
if ( ! defined( 'WP_PLUGIN_URL' ) ) define( 'WP_PLUGIN_URL', WP_CONTENT_URL. '/plugins' );
if ( ! defined( 'WP_PLUGIN_DIR' ) ) define( 'WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins' );

if ( basename(dirname(__FILE__)) == 'plugins' )
	define("STOCKIST_SYNC_DIR",'');
else define("STOCKIST_SYNC_DIR" , basename(dirname(__FILE__)) . '/');
define("STOCKIST_SYNC_PATH", WP_PLUGIN_URL . "/" . STOCKIST_SYNC_DIR);

/*

******** BEGIN PLUGIN FUNCTIONS ********

*/

// ------ Sync Magento Stockists with WP Store Locator Stockists

if ( ! wp_next_scheduled( 'stockistSyncCron' ) ) {
  wp_schedule_event( time(), 'daily', 'stockistSyncCron' );
}

// add_action( 'stockistSyncCron', 'stockistSync' );

function stockistSync() {
	
	if(isset($_POST['mexco-import'])) {
	
	// Include Magento
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/mexco/shop/app/Mage.php');
	umask(0);
	Mage::app();
	
	// Retrieve Magento Customer Model
	
	$stockists = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('*');
		// ->addFieldToFilter('group_id', 2); // Filter by customer group id. Likely the best method...
				
	// Loop Through Customers / Stockists
				
	foreach ($stockists as $stockist) {
		
		// Get Customer ID and load additional data.
		
		$row = $stockist->getData();

		$customerId = $row['entity_id'];
		$customer = Mage::getModel('customer/customer')->load($customerId);
		
		// Can be used to filter by customer group name. Probably more heavy and inappropriate with retrieving via session...
		
		// $roleId = Mage::getSingleton('customer/session')->getCustomerGroupId();
		// $role = Mage::getSingleton('customer/group')->load($roleId)->getData('customer_group_code');
		// $role = strtolower($role);
		
		// Loop Through Addresses - Break After First Item (This could be less crude, run getDefaultAddress if possible to regate the foreach)

		foreach ($customer->getAddresses() as $address) {
			
			$customerAddress = $address->toArray();
			break;

		}
		
		// Declare Stockist Vars
		
		$company = $customerAddress['company'];
		$street = $customerAddress['street'];
		$city = $customerAddress['city'];
		$postcode = $customerAddress['postcode'];
		$country = Mage::app()->getLocale()->getCountryTranslation( $customerAddress['country_id']);
			
		// Create Stockist in Wordpress
		
		
		//$post_id = get_page_id_by_title($company, 'wpsl_stores');
		$post_id = get_page_by_title( $company, OBJECT, 'wpsl_stores');
		
		
		if (!empty($post_id)) {
			
			$post_id = $post_id->ID;
			
		}
		
		
		if (empty($post_id)) {
		
			$stockistCreate = array(
			  'post_type'     => 'wpsl_stores',
			  'post_title'    => $company,
			  'post_status'   => 'publish',
			  'post_author'   => 1
			);
			
			// Insert Stockist into the WP Database if it doesn't already exist - Need Appropriate Conditional
			
			$post_id = wp_insert_post($stockistCreate);
			
		}
		
		
		if (!empty($post_id)) {
			
			// Add Custom Fields
			
			inspired_stockist_update_meta($post_id, 'wpsl_address', $street, true);
			inspired_stockist_update_meta($post_id, 'wpsl_city', $city, true);
			inspired_stockist_update_meta($post_id, 'wpsl_zip', $postcode, true);
			inspired_stockist_update_meta($post_id, 'wpsl_country', $country, true);
			
		}
		
	}
	
	//wp_reset_postdata();
	
	}

}

add_action('init', 'stockistSync');

function outputTest() {
	
if(!empty($_GET['stockist-sync'])) {
	
	echo 'milker';
	
}
	
}

add_action('init', 'outputTest');

function get_page_id_by_title($title, $type) {
	
	global $wpdb;
	
	$post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %d AND post_type = %a ", $title, $type ));
		
	if ($post) { return $post; }
		
    return null;
}

// custom update post field switch - add remove etc

function inspired_stockist_update_meta($post_id, $field_name, $value = '') {
 
	if (empty($value) OR !$value) {

		delete_post_meta($post_id,$field_name);

	} else if (!get_post_meta($post_id,$field_name)){

		add_post_meta($post_id,$field_name,$value);

	} else {

		update_post_meta($post_id,$field_name,$value);

	}
}

// ------------------- Magento Stockist Output Check (used for testing purposes)

function stockistTest() {
	
	// Retrieve Mage Customer Model
	
	$stockists = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('*');
				
	// Loop Through Customers / Stockists
	
	$customerCounter = 0;
	
	echo '<ul>';
				
	foreach ($stockists as $stockist) {
		
		++$customerCounter;
			
		$row = $stockist->getData();

		$customerId = $row['entity_id'];
		$customer = Mage::getModel('customer/customer')->load($customerId);
		
		// Loop Through Addresses - Break After First Item

		foreach ($customer->getAddresses() as $address) {
			
			$customerAddress = $address->toArray();
			break;

		}
		
		// Declare Stockist Vars
		
		$company = $customerAddress['company'];
		$street = $customerAddress['street'];
		$city = $customerAddress['city'];
		$postcode = $customerAddress['postcode'];
		$country = Mage::app()->getLocale()->getCountryTranslation( $customerAddress['country_id']);
		
		// Output List - break on 10
			
		echo '<li>';
		
			echo '<h3>' . $company . '</h3>';
			echo '<address>' . $street . '<br />' . $city . '<br />' . $postcode . '<br />' . $country . '</address>';
		
		echo '</li>';
		
		if($customerCounter == 10) {
			break;
		}
		
	}
	
	echo '</ul>';

}	

// add_action('foundationpress_after_header', 'stockistTest');

// Admin Menu Item

function inspired_stockist_sync_add_pages() {

	add_options_page( 'StockistSync', 'Stockist Sync', 'read', 'stockist_sync', 'inspired_stockist_sync_overview');

}

add_action('admin_menu', 'inspired_stockist_sync_add_pages');

// Admin Page Content

function inspired_stockist_sync_overview() {
?>
<div class="wrap">
	<h2>Inspired Stockist Sync - Mexco</h2>
	<p>To Run the Import please click the button below. Only run the Stockist Sync if you need to push a manual import/sync as it will check every 24 hours if there are new stockists and will Sync them automatically.</p>
	<form method="post" action="<?php the_permalink(); ?>">
		<input class="button" name="mexco-import" type="submit" value="Sync Stockists with WP Store Locator" />
	</form>
	<?php if(isset($_POST['mexco-import'])): ?><p>Stockist Sync Complete</p><?php endif; ?>
	
</div>
<?php
exit;
}

?>