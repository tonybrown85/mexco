<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mexwp');

/** MySQL database username */
define('DB_USER', 'mexco');

/** MySQL database password */
define('DB_PASSWORD', '1n5p1r3d');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Etj)Tk{%BdD1-.4Zqg0 }`=eRFoLBq`%xWd,S(<u{o^95;Dz+UhuU6kB}^2+YsR^');
define('SECURE_AUTH_KEY',  'A_XjvMm2{02O%TF#csxK+!U^nK#Fv*M!PPrSsey4hym-@v4<~x9]r|[ndknT{?Vv');
define('LOGGED_IN_KEY',    'x|:au8g,MA^pE/MH:%3).S$:e-=IjZK4CL/;Kpdl+YRc%(g~JILh+=QD%-|H/a|}');
define('NONCE_KEY',        '0O`-Qq6?u2PGP<OJBP|JG+>t&` gu!kqCHF+R;${AC8fVx.&it-I2=_5$I!U|-~4');
define('AUTH_SALT',        ')}Y#$JC8ie@IX5ki~k2| 8yIytS5ged+2yBVpqQLW^_vC^|gqgsUe3yEJtP_Zxu;');
define('SECURE_AUTH_SALT', '!Z+Xhz|g;$sFy~T]YC/oy*K0$LxQ.ZwHL 7JGZtz1e6>o+1rlh}4$J7/b|g+P},0');
define('LOGGED_IN_SALT',   '7v-~:Q-GvoF0=NX<3Gr+^4x{)bU8&%7-m<RPu1<BAE?+aqRmbHUV+?^4W ot74&P');
define('NONCE_SALT',       '#(wWIv^^eU8TteX%c{@(**yOq2o|U7o8/^P5:VbV9{+g{[d5 E-||<$:`l5)BrNT');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
